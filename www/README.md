# GlitchMV

Updating NWJS (speed upgrade) for RPG Maker MV

Visit https://nwjs.io/ and download the latest sdk version.

Go to your RPG Maker MV folder and decompress the file you downloaded. Copy it's contents into the nwjs-win-test folder. Select to replace all files. Delete the file Game.exe and rename the file nw.exe to Game.exe.

Enjoy!
