VERY IMPORTANT INFO:

Everything in this game is programmed intentionally, but some elements are not typical of RPG Maker games. Please do not be alarmed because we guarantee that everything has been tested thoroughly to ensure a safe gaming experience for your computer.

For data entry, do not press CapsLock to type in capital letters. Shift works. This is for the probably 2 people other than one of our devs who does that. ;)

There are some differences between this and the VX Ace version. The main difference is that there is a completely different save system. In order for your game to function properly, please do not mess with the save files. Deletion of any files other than the numbered save files could cause your game to malfunction. Don't do it. If you want a complete reset of your entire game, you have to delete or move everything in your save folder.

We will include an option for SAFE MODE. Safe Mode is a mode in which the game will run without jumpscares or some of the scarier elements. It's important that you switch that to ON because it will be OFF automatically, should you decide to use it. You will be prompted in the opening. Let's Players, we recommend you don't use it because it's much more fun that way. This game is not explicit and is probably a solid T-rating (though there's definitely more swearing than the first game).

There is also a HELP MODE for people with sensory disorders. This includes people who are dyslexic or who have sensitivity to noise. Accents will be removed from dialogue and larger words will be simplified. Colorblind players do not need this; the game is set up to accommodate you in the original mode.

Installation of this game comes with the possibility of setting Anti loose on your computer. We've isolated him to the game files, but remember that Wireland is also on your game files. It's up to you to keep it safe.